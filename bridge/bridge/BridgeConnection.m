//
//  BridgeConnection.m
//  bridge
//
//  Created by Sridatta Thatipamala on 4/26/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "bridge.h"
#import "BridgeConnection.h"
#import "GCDAsyncSocket.h"
#import "BridgeJSONCodec.h"
#import "BridgeDispatcher.h"
#import "BridgeUtils.h"
#import "BridgeSocket.h"
#import "BridgeTCPSocket.h"
#import "BridgeSocketBuffer.h"
#import "BridgeClient.h"

static const float INITIAL_RECONNECT_BACKOFF = 0.5;

@implementation BridgeConnection

@synthesize host, port, clientId, secret;

- (id)initWithApiKey:(NSString*)anApiKey options:(NSDictionary*)options bridge:(Bridge*)aBridge
{
    self = [super init];
    if (self) {
      isRetrying = NO;
      host = [options objectForKey:@"host"];
      NSNumber* portOption = [options objectForKey:@"port"];
      if(portOption == nil) {
        port = -1;
      } else {
        port = [portOption intValue];
      }
      
      reconnect = [[options objectForKey:@"reconnect"] boolValue];
      
      apiKey = [anApiKey copy];
      
      NSString* redirectorString = [options objectForKey:@"redirector"];
      secure = NO;
      if([[options objectForKey:@"secure"] boolValue] == YES) {
        secure = YES;
        redirectorString = [options objectForKey:@"secureRedirector"];
      }
      
      redirectorURL = [[NSURL URLWithString:redirectorString] retain];
      
      bridge = aBridge;
      responseData = [[NSMutableData dataWithLength:0] retain];
      
      socket_buffer = [[BridgeSocketBuffer alloc] init];
      sock = [socket_buffer retain];
      
      reconnectBackoff = INITIAL_RECONNECT_BACKOFF;
      consecutiveFailures = 0;
    }
    
    return self;
}

-(void) dealloc
{  
  [host release];
  
  [clientId release];
  [secret release];
  [apiKey release];
  
  [redirectorURL release];
  [responseData release];
  [super dealloc];
}

/*
 @brief Connect to the Bridge server using the network information provided to initializer
 */
- (void) start
{
  isRetrying = NO;
  if(host == nil || port == -1){
    [self redirector];
  } else {
    [self establishConnection];
  }
}

-(void) redirector {
  NSURL* connectionURL = [redirectorURL URLByAppendingPathComponent:[NSString stringWithFormat:@"redirect/%@", apiKey]];
  NSLog(@"URL is: %@", connectionURL);
  NSURLRequest *request = [NSURLRequest requestWithURL:connectionURL];
  [NSURLConnection connectionWithRequest:request delegate:self];
}

-(void) establishConnection {
  NSLog(@"Starting TCP connection %@ , %d", host, port);
  
  // Initialize a TCP connection. It will call back once ready.
  [[BridgeTCPSocket alloc] initWithConnection:self isSecure:secure];
}

-(void) send:(NSData*) data
{  
  [sock send:data];
}

#pragma mark NSURL delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
  [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
  [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
  // Show error
  ++consecutiveFailures;
  NSLog(@"error: %@", [error localizedDescription]);
  
  if (consecutiveFailures > 3)
  {
    [bridge _onConnectionError:error];
  }
  [self retryAfterDelay];

}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  // Once this method is invoked, "responseData" contains the complete result
  NSDictionary* jsonObj = [BridgeJSONCodec parseRedirector:responseData];
  
  NSDictionary* data = [jsonObj objectForKey:@"data"];
  if(data && [data objectForKey:@"bridge_host"] != nil && [data objectForKey:@"bridge_port"] != nil)
  {
    host = [[data objectForKey:@"bridge_host"] copy];
    port = [((NSString*)[data objectForKey:@"bridge_port"]) intValue];
    
    [self establishConnection];
  } else {
    NSError* error = [[NSError alloc] initWithDomain:@"com.getbridge" code:1 userInfo:[NSDictionary dictionaryWithObject:@"Could not find host and port in JSON body" forKey:@"message"]];
    ++consecutiveFailures;
//    NSLog(@"Could not find host and port in JSON body");
    if (consecutiveFailures > 3) {
      [bridge _onConnectionError:error];
    }
    [error release];
    [self retryAfterDelay];

    return;
  }
}

-(void)onOpenFromSocket:(id<BridgeSocket>)socket
{
  NSLog(@"Beginning handshake");
  // Send a connect message
  NSData* connectString = [BridgeJSONCodec createCONNECTWithId:[bridge clientId] secret:secret apiKey:apiKey];
  
  // Send to the socket directly. Do not buffer
  [socket send:connectString];
}

-(void)onCloseWithError:(NSError *)error
{
  NSLog(@"Connection closed. Error: %@", error.debugDescription);
  
  [sock release];
  sock = [socket_buffer retain];
    
  // While we continually attempt to reconnect when we get errors, some errors
  // can be taken care of without even telling the user that there is a problem
  if (error.domain == GCDAsyncSocketErrorDomain && error.code == 7)
  {
      // "Socket closed by remote peer" - this happens when you come back to the app
      // after it was in the background.
  }
  else
  {
      ++consecutiveFailures;
      if (consecutiveFailures > 3)
      {
          [bridge _onConnectionError:error];
      }
  }
    
  [self retryAfterDelay];
}

-(void)retryAfterDelay
{
  if (isRetrying) return;
  isRetrying = YES;
  SEL connectSelector = @selector(start);
  NSInvocation* inv = [NSInvocation invocationWithMethodSignature:[self methodSignatureForSelector:connectSelector]];
  [inv setSelector:connectSelector];
  [inv setTarget:self];
  [NSTimer scheduledTimerWithTimeInterval:reconnectBackoff invocation:inv repeats:NO];
  
  reconnectBackoff *= 2;
  if (reconnectBackoff >= 5) reconnectBackoff = 5;
  
}

-(void)onConnectMessage:(NSString*)message fromSocket:(id<BridgeSocket>) socket
{
    
  NSArray* chunks = [message componentsSeparatedByString:@"|"];
  
  if ([chunks count] == 2) {
    NSLog(@"client_id received: %@", [chunks objectAtIndex:0]);
    
    [clientId release];
    clientId = [[chunks objectAtIndex:0] retain];
    
    [secret release];
    secret = [[chunks objectAtIndex:1] retain];
    
    [socket_buffer processQueueIntoSocket:socket withClientId:clientId];
    [sock release];
    sock = [socket retain];
    
    NSLog(@"Handshake complete");
    
    reconnectBackoff = INITIAL_RECONNECT_BACKOFF;
    consecutiveFailures = 0;
    
    [bridge _ready];
  } else {
    [self onMessage:message fromSocket:socket];
  }

}

-(void) onMessage:(NSString*)message fromSocket:(id<BridgeSocket>)socket
{
  NSDictionary* root = [BridgeJSONCodec parseRequestString:message bridge:bridge];
  
  BridgeRemoteObject* destination = [root objectForKey:@"destination"];
  if(destination == nil) {
    NSLog(@"No destination in message %@", message);
    return;
  }
  
  NSString* source = [root objectForKey:@"source"];
  if (source != nil) {
    [bridge setContext:[[BridgeClient alloc] initWithBridge:bridge clientId:source]];
  }
  
  NSArray* arguments = [root objectForKey:@"args"];
  [bridge.dispatcher executeUsingReference:destination withArguments:arguments];

}

@end
